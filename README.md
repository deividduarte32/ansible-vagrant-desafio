# Desafio ansible + vagrant

## Atividades requeridas no desafio

### Criar máquina virtual com vagrant no virtualbox, em seguida criar uma role ansible que instale os pacotes:

Instalar pacotes:
vim
curl
telnet
unzip
wget
net-tools
htop
nmap
nginx

### Definir nome da máquina

### Criar um usuário com o seu nome

### Copiar um arquivo index.html

### Baixe o repositório
`git clone https://gitlab.com/deividduarte32/ansible-vagrant-desafio.git`

### Entre no diretório
`cd ansible-vagrant-desafio/Documentos/Vagrant`

### Provisionar máquina virtual
`vagrant up`
